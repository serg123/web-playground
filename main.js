import IndexPage from './components/IndexPage.js';
import PlaygroundPage from './components/PlaygroundPage.js';
import ModalFactory from './components/ModalFactory.js';

const router = new VueRouter({routes:[
    { path: '/', name: 'index', component: IndexPage },
    { path: '/playground/:id', name: 'playground', component: PlaygroundPage, props:true }
]})
window.app = new Vue({
    router:router,
    el:'#app',
    components:{'modal-factory':ModalFactory},
    template:'<div><router-view></router-view><modal-factory ref="modals"></modal-factory></div>',
    created(){
        this.$router.push(document.location.href.split('#')[1]||'/');
    },
    mounted(){
        Vue.prototype.$modals=this.$refs.modals;
    }
})