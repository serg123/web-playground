# Description

### A simple browser playground that allows:
- include 3rd party assets (outer .js and .css files)
- edit HTML body code
- edit CSS styles
- edit Java Script
- run preview in iframe
- save changes to server with description
- load previously saved data for editing
- delete data
- search by description

### Techs:
- Vue.js
- vue-router
- Font Awesome icons
- Codemirror
- pure PHP backend using MySQL via mysqli
- made in the form of PWA
