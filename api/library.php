<?php
$HOST = "localhost";
$USER = "global";
$PASSWORD = "global";
$DB = "global";
$TABLE = "web_playground__library";

header("Access-Control-Allow-Origin: *");
$conn = new mysqli($HOST, $USER, $PASSWORD, $DB);

switch($_SERVER['REQUEST_METHOD']){
    case 'GET':
        if(has_id($_REQUEST))
            get($_REQUEST['id']);
        else
            all();
        break;
    case 'POST':
        $json = json_decode(file_get_contents("php://input"),true);
        if(isset($json['description']) && isset($json['code'])){
            if(has_id($_REQUEST))
                update($_REQUEST['id'], $json['description'], $json['code']);
            else
                create($json['description'],$json['code']);
        }
        else if(has_id($_REQUEST))
            delete($_REQUEST['id']);
        else
            http_response_code(400); exit();
        break;
    default:
        http_response_code(400); exit();
}


function has_id($args){
    return isset($args['id']) && ((int)$args['id'])>0;
}

function create($description, $code){
    global $conn,$TABLE;
    $sql = "insert into {$TABLE}(description,code) values('{$description}','{$conn->escape_string($code)}')";
    $conn->query($sql);
    echo  $conn->insert_id;
}

function update($id, $description, $code){
    global $conn,$TABLE;
    $sql = "update {$TABLE} set description='{$description}', code='{$conn->escape_string($code)}' where id={$id}";
    $conn->query($sql);
}

function delete($id){
    global $conn,$TABLE;
    $sql = "delete from {$TABLE} where id={$id}";
    $conn->query($sql);
}

function get($id){
    global $conn,$TABLE;
    $sql = "select * from {$TABLE} where id={$id}";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    echo json_encode($row);
}

function all(){
    global $conn,$TABLE;
    $sql = "select id,description from {$TABLE}";
    $result = $conn->query($sql);
    $rows=array();
    while($row = $result->fetch_assoc())
        $rows[]=$row;
    echo json_encode($rows);
}

$conn->close();