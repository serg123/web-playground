create database global;
use global;
create user 'global'@'localhost' identified by 'global';
grant all on global.* TO 'global'@'localhost';

create table web_playground__library(
  id int auto_increment primary key,
  description varchar(300),
  code text
);