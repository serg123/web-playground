export default class PreviewControler{
    constructor(iframe){
        this.iframe=iframe;
    }
    update(html,css,js,inc){
        inc = inc.split(/(?: +|\n+)/);
        inc = inc.reduce(function(result,v){
            v=v.trim();
            if(v.lastIndexOf('.css')==v.length-4) {
                result.push(`<link rel="stylesheet" href="${v}">`);
            }
            else if(v.lastIndexOf('.js')==v.length-3)
                result.push(`<script src="${v}">`);
            return result;
        },[]);
        let code=`data:text/html,<!DOCTYPE html><!--${Math.random()}-->
<html>
<head>
${inc.join('\n')}
<script>
${js}
</script>
<style>
${css}
</style>
</head>
<body>
${html}
</body>
</html>`;
        this.iframe.src=code.replace(/#/g,'%23');
    }
}