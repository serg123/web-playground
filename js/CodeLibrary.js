export default class CodeLibrary{
    constructor(apiUrl='api/library.php'){
        this.apiUrl=apiUrl;
    }
    async create(description='',code=''){
        return new Promise((resolve,reject)=>{
            code = JSON.stringify(code);
            let method='post';
            let headers={'Content-type': 'application/json'};
            let body = JSON.stringify({description, code});
            fetch(this.apiUrl,{method,headers,body}).then(response=>response.text()).then(function(data){
                resolve(data);
            })
        });
    }
    async all(){
        return new Promise((resolve,reject)=>{
            fetch(this.apiUrl).then(response=>response.json()).then(function(data){
                resolve(data);
            })
        });
    }
    async get(id){
        return new Promise((resolve,reject)=>{
            fetch(`${this.apiUrl}?id=${id}`).then(response=>response.json()).then(function(data) {
                try { data.code = JSON.parse(data.code); }
                catch(e){ data.code='' };

                resolve(data);
            })
        });
    }
    async update(id,description='',code=''){
        return new Promise((resolve,reject)=>{
            code = JSON.stringify(code);
            let method='post';
            let headers={'Content-type': 'application/json'};
            let body = JSON.stringify({description, code});
            fetch(`${this.apiUrl}?id=${id}`,{method,headers,body}).then(function(){
                resolve();
            })
        });
    }
    async delete(id){
        return new Promise((resolve,reject)=>{
            let method='post';
            fetch(`${this.apiUrl}?id=${id}`,{method}).then(function(){
                resolve();
            })
        });
    }
}