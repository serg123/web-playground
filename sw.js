VERSION="1";
NO_CACHE_URL='api/library.php';
this.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(VERSION).then(function(cache) {
            return cache.addAll([
                'manifest.json',
                'icon.png',
                'index.html'
            ]);
        })
    );
});
this.addEventListener('activate', function(event) {
    event.waitUntil(caches.keys().then(function(keyList) {
        return Promise.all(keyList.map(function(key) {
            if (key!=VERSION) {
                return caches.delete(key);
            }
        }));
    }));
});
this.addEventListener('fetch', function(event) {
    event.respondWith(caches.match(event.request).then(
        function(response) {
            return response || fetch(event.request).then(function(response) {
                if(event.request.url.includes(NO_CACHE_URL))
                    return response;
                else return caches.open(VERSION).then(function(cache) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            });
        })
    );
});
