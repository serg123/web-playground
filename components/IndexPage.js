import ListComponent from './ListComponent.js'
import CodeLibrary from '../js/CodeLibrary.js'

export default {
    template: `
    <div>
    <div class="background"></div>
    <nav>
        <button class="fa fa-plus" @click="navigate()" title="new"></button>
        <input type="text" placeholder="filter" v-model="filter"></input>
    </nav>
    <section>
        <list-component :items="filteredItems" itemName="description" @click="navigate"></list-component>
    </section>
    </div>
    `,
    components:{'list-component':ListComponent},
    data:function(){
        return {
            items:[],
            filter:''
        };
    },
    created(){
        this.library=new CodeLibrary();
        this.update();
    },
    methods:{
        navigate(item){
            if(item)
                this.$router.push({name:'playground',params:{id:item.id}})
            else
                this.$router.push({name:'playground',params:{id:0}})
        },
        update(){
            this.library.all().then(data=>this.items=data);
        }
    },
    computed:{
        filteredItems(){
            let a=this.items.reduce((items,v)=>{
                if(v.description.includes(this.filter))
                    items.push(v)
                return items;
            }, []);
            return a;
        }
    }
}