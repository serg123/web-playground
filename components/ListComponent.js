export default {
    template: `
    <div class="vertical-list">
        <div class="li" v-for="item in items" @click="$emit('click',item)">{{item[itemName]}}</div>
    </div>
    `,
    props:['items','itemName'],    //id, name
}