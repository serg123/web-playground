export default {
    template: `
    <div class="modal-bg" @click="close" :style="{display:display}">
        <div class="modal-head" @click="$event.stopPropagation()">
            {{title}}
            <a class="fa fa-times modal-btn" @click="close"></a>
        </div>
        <div class="modal-body" @click="$event.stopPropagation()">
            {{msg}}
        </div>
        <div class="modal-footer" @click="$event.stopPropagation()">
            <button @click="close" v-if="closeBtn">Close</button>
            <button @click="cancel" v-if="cancelBtn">Cancel</button>
            <button style="margin-right:20px; padding:0 20px" @click="ok" v-if="okBtn">OK</button>
        </div>
    </div>
    `,
    data(){
        return {
            msg:'',
            title:'',
            display:'none',
            okBtn:false,
            cancelBtn:false,
            closeBtn:false
        }
    },
    methods:{
        alert(msg, onClose){
            this.onClose=onClose;
            this.msg=msg;
            this.okBtn=false; this.cancelBtn=false; this.closeBtn=true;
            this.title="Alert";
            this.display='block';
            document.querySelector(':focus').blur();
        },
        confirm(msg, onOk, onCancel, onClose){
            this.onClose=onClose; this.onOk=onOk; this.onCancel=onCancel;
            this.msg=msg;
            this.closeBtn=false; this.okBtn=true; this.cancelBtn=true;
            this.title="Confirm!";
            this.display='block';
            document.querySelector(':focus').blur();
        },
        close(){
            if(this.onClose)
                this.onClose();
            this.display='none';
        },
        ok(){
            if(this.onOk)
                this.onOk();
            this.display='none';
        },
        cancel(){
            if(this.onCancel)
                this.onCancel();
            this.display='none';
        }
    }
}