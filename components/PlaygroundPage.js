import PreviewController from '../js/PreviewController.js'
import CodeLibrary from '../js/CodeLibrary.js'

export default {
    template: `
    <div>
    <div class="background"></div>
    <nav>
        <button class="fa fa-bars" @click="back" title="back to list"></button>
        <input id="description" type="text" placeholder="description" style="width:100%; max-width:600px"/>
        <button class="fa fa-save" @click="update" title="save"></button>
        <button class="fa fa-trash" @click="del" title="delete"></button>
    </nav>
    <section style="max-width:100vw">
        <div class="li">
            <div class="tabs">
                <div class="li" @click="toggleTab('html')" :class="{active:html}">HTML</div>
                <div class="li" @click="toggleTab('css')" :class="{active:css}">CSS</div>
                <div class="li" @click="toggleTab('js')" :class="{active:js}">JS</div>
                <div class="li" @click="toggleTab('inc')" :class="{active:inc}">Inc</div>
                <div class="li" @click="play" :class="{active:run}"><span class="fa fa-play" style="color:#070"></span></div>
            </div>
            <textarea id="htmlEditor"></textarea>
            <textarea id="cssEditor"></textarea>
            <textarea id="jsEditor"></textarea>
            <textarea id="incEditor"></textarea>
            <iframe></iframe>
        </div>
    </section>
    </div>
    `,
    created(){
        this.library=new CodeLibrary();
        if(this.id>0)
            this.library.get(this.id).then(data=>this.loadedData=data);
    },
    mounted(){
        if(this.id>0)
            this.loadToEditor(this.loadedData);
        this.previewController = new PreviewController(document.querySelector('iframe'));
        this.htmlEditor=CodeMirror.fromTextArea(document.getElementById('htmlEditor'), {lineNumbers: true, mode:"htmlmixed"});
        this.cssEditor=CodeMirror.fromTextArea(document.getElementById('cssEditor'), {lineNumbers: true, mode:"css"});
        this.jsEditor=CodeMirror.fromTextArea(document.getElementById('jsEditor'), {lineNumbers: true, mode:"javascript"});
        this.incEditor=CodeMirror.fromTextArea(document.getElementById('incEditor'), {lineNumbers: true});
        this.toggleTab('html');
    },
    props:['id'],
    data(){
        return {
            html: false,
            css: false,
            js: false,
            inc: false,
            run: false,
            loadedData: null
        }
    },
    methods:{
        play(){
            this.toggleTab('run');
            this.previewController.update(this.htmlEditor.getValue(), this.cssEditor.getValue(), this.jsEditor.getValue(), this.incEditor.getValue());
        },
        toggleTab(tab){
            this.html=this.css=this.js=this.inc=this.run=false;
            this[tab]=true;
            document.querySelectorAll('textarea+*,iframe').forEach(node=>node.style.display='none');
            let editor=document.querySelector('#'+tab + 'Editor+*');
            if(editor) {
                editor.style.display = 'block';
                this[tab+'Editor'].refresh();
            }
            else
                document.querySelector('iframe').style.display='block';
        },
        back(){
            this.$router.push({name:'index'});
        },
        loadToEditor(data){
            if(data && this.incEditor){ //this will be satisfied after mounting and fetching
                document.getElementById('description').value=data.description || '';
                if(data.code){
                    this.htmlEditor.getDoc().setValue(data.code.html || '');
                    this.cssEditor.getDoc().setValue(data.code.css || '');
                    this.jsEditor.getDoc().setValue(data.code.js || '');
                    this.incEditor.getDoc().setValue(data.code.inc || '');
                }
                else {
                    this.htmlEditor.getDoc().setValue('');
                    this.cssEditor.getDoc().setValue('');
                    this.jsEditor.getDoc().setValue('');
                    this.incEditor.getDoc().setValue('');
                }
            }
        },
        update(){
            let description=document.getElementById('description').value;
            let code={
                html: this.htmlEditor.getValue(),
                css: this.cssEditor.getValue(),
                js: this.jsEditor.getValue(),
                inc: this.incEditor.getValue(),
            }
            if(this.id>0)
                this.library.update(this.id, description, code).then(()=>this.$modals.alert('All changes were saved!'));
            else
                this.library.create(description, code).then(id=>{
                    this.$modals.alert('A new item was created!');
                    this.navigate(id);
                });
        },
        del(){
            if(this.id>0)
                this.$modals.confirm('Are you sure to delete this?',()=>{this.library.delete(this.id).then(()=>this.back())})
        },
        navigate(id){
            this.$router.push({name:'playground',params:{id}})
        },
    },
    watch:{
        loadedData(newValue){
            this.loadToEditor(newValue);
        }
    }
}